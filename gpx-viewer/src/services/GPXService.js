let axios = require('axios')
let geocodingService = require('./GeocodingService')

let gpxServerEndPoint = 'http://localhost:3000'

let getSummary = function (activityId) {
    return axios.get(gpxServerEndPoint + '/activity/' + activityId + '/summary')
}

let getDetail = function (activityId) {
    return axios.get(gpxServerEndPoint + '/activity/' + activityId + '/detail')
}

let getStops = function (stopRecords, callback) {

    let stops = []

    stopRecords.forEach(record => {
        geocodingService.getReverseGeocoding(record.lat, record.lon)
        .then(response => {
            let location = response.data.results[0].locations[0]
            let address = location.adminArea3
            if (location.adminArea5) {
                address += ' - ' + location.adminArea5
            }
            if (location.street) {
                address += ' - ' + location.street
            }
            //console.log(location)
            stops.push({ address: address, record: record })
        }).catch(e => {
            console.log(e)
            throw e
        })
    })

    let interval = setInterval(function() {
        if (stops.length === stopRecords.length) {
            callback(stops)
            clearInterval(interval)
        }
    }, 500)
}

module.exports.getSummary = getSummary
module.exports.getDetail = getDetail
module.exports.getStops = getStops