let axios = require('axios')

let mapQuestKey = 'r3BTR4SDceAoZ1qMkoSx4haqAPCG7P0I'

let getReverseGeocoding = function (lat, lon) {
    return axios.get('https://www.mapquestapi.com/geocoding/v1/reverse?key='+ mapQuestKey +'&location=' + lat +'%2C' + lon + '&outFormat=json&thumbMaps=false')
}

module.exports.getReverseGeocoding = getReverseGeocoding