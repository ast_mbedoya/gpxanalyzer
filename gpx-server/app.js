'use strict'

const express = require('express')
const server = express()
const activityRouter = require('./src/routers/ActivityRouter')

function setCors(response) {
    response.header("Access-Control-Allow-Origin", "*")
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
}

server.use((req, res, next) => {
    setCors(res)
    next()
})

server.use('/activity', activityRouter)

server.listen(3000, () => console.log('server listening...'))

