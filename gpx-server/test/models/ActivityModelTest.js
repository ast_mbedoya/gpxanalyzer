let assert = require('assert'),
    activityModel = require('../../src/models/ActivityModel')

describe('ActivityModel', () => {
    describe('#getTimeDifferenceInMiliseconds', () => {
        it('difference should be 2000', () => {
            let startDate = "2018-06-09T10:17:09Z",
                endDate = "2018-06-09T10:17:11Z"

            let difference = activityModel.getTimeDifferenceInMiliseconds(startDate, endDate)
            assert.equal(difference, 2000)
        })
    })

    describe('#timeDifferenceIsBig', () => {
        it('equal or more than 5 seconds should be big', () => {
            let startDate = "2018-06-09T10:17:09Z",
                endDate = "2018-06-09T10:17:14Z"

            let differenceIsBig = activityModel.timeDifferenceIsBig(startDate, endDate)
            assert.equal(differenceIsBig, true)
        })

        it('less than 5 seconds should not be big', () => {
            let startDate = "2018-06-09T10:17:09Z",
                endDate = "2018-06-09T10:17:11Z"

            let differenceIsBig = activityModel.timeDifferenceIsBig(startDate, endDate)
            assert.equal(differenceIsBig, false)
        })
    })

    describe('#getDetail', () => {
        it('should contain stopRecords', () => {
            let activityId = "1"

            activityModel.getDetail(activityId, detail => {
                assert.ok(detail.stopRecords.length > 0)
                done()
            })
        })
    })
})