var express = require('express')
var router = express.Router()
var activityModel = require('../models/ActivityModel')

router.use(function timeLog(req, res, next) {
    console.log('/gpx - Incoming request time: ', Date.now())
    next()
})

router.get('/:activityid/summary', (req, res) => {
    return activityModel.getSummary(req.params.activityid, summary => res.json(summary))
})

router.get('/:activityid/detail', function (req, res) {
    return activityModel.getDetail(req.params.activityid, detail => res.json(detail))
})

module.exports = router