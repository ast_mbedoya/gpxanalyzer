const fs = require('fs')
const parseString = require('xml2js').parseString
const path = require('path')

let openXml = function (filePath, callback) {
    fs.readFile(path.resolve(__dirname, filePath), 'utf8', (error, xml) => {
        if (error)
            throw error

        parseString(xml, (err, xml) => {
            if (err)
                throw err

            callback(xml)
        })
    })
}

module.exports.openXml = openXml