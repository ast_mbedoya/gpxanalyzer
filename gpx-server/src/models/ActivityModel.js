'use strict'

const fileReader = require('../util/FileReader')
const geoMath = require('../util/GeoMath')

const fileNames = ['../../static/Manizales.gpx', '../../static/Guatape.gpx']

let bigTimeInSeconds = 60
let milisecondsToSecondsRatio = 1/1000
let cachedXmlString = null

/* UTILS  */

let getSpeedRecords = function (records) {

    let speedRecords = []

    for (let i = 1; i < records.length; i++) {
        const element = records[i]
        let speedInKph = geoMath.getDistanceFromLatLonInKm(+records[i - 1].$.lat, +records[i - 1].$.lon, +element.$.lat, +element.$.lon) * 3600
        speedRecords.push(speedInKph)
    }

    return speedRecords
}

function getAverageSpeed(speedRecords) {
    let sum = 0
    for (let i = 0; i < speedRecords.length; i++) {
        const element = speedRecords[i]
        sum += element
    }
    let averageSpeed = sum / speedRecords.length

    return averageSpeed
}

let getTimeDifferenceInMiliseconds = function (startDate, endDate) {
    return new Date(endDate).getTime() - new Date(startDate).getTime()
}

let timeDifferenceIsBig = function (startDate, endDate) {
    let differenceInMiliseconds = getTimeDifferenceInMiliseconds(startDate, endDate)
    return (differenceInMiliseconds*milisecondsToSecondsRatio).toFixed(2) >= bigTimeInSeconds
}

let groupStopRecords = function (stopRecords) {
    return stopRecords
}

let addStopRecord = function (stopRecords, newRecord) {
    newRecord.id = stopRecords.length+1
    stopRecords.push(newRecord)
    return stopRecords
}

function getStopRecords(records) {
    let stopRecords = []

    if (records && records.length > 0) {

        //Add First GPS Record
        stopRecords = addStopRecord(stopRecords, {
            lat: records[0].$.lat,
            lon: records[0].$.lon,
            elevation: records[0].ele[0],
            time: records[0].time[0],
            pauseTimeInSeconds: 0
        })

        for (let i = 1; i < records.length; i++) {
            const element = records[i]
            if (timeDifferenceIsBig(records[i-1].time[0], records[i].time[0])) {
                stopRecords = addStopRecord(stopRecords, {
                    lat: element.$.lat,
                    lon: element.$.lon,
                    elevation: element.ele[0],
                    time: records[i-1].time[0],
                    pauseTimeInSeconds: getTimeDifferenceInMiliseconds(records[i-1].time[0], records[i].time[0])*milisecondsToSecondsRatio
                })
            }
        }

        //Add Last GPS Record
        stopRecords = addStopRecord(stopRecords, {
            lat: records[records.length - 1].$.lat,
            lon: records[records.length - 1].$.lon,
            elevation: records[records.length - 1].ele[0],
            time: records[records.length - 1].time[0],
            pauseTimeInSeconds: 0
        })
    }

    return groupStopRecords(stopRecords)
}

let getFilenameByActivity = function (activityId) {
    return fileNames[activityId-1]
}

let getActivityXml = function (activityId, callback) {

    let fileName = getFilenameByActivity(activityId)

    if (!cachedXmlString) {
        fileReader.openXml(fileName, function (xml) {
            cachedXmlString = xml
            callback(cachedXmlString)
        })
    }else{
        callback(cachedXmlString)
    }
}

let getRecords = function (activityId, callback) {
    getActivityXml(activityId, function (xml) {
        let records = xml.gpx.trk[0].trkseg[0].trkpt
        callback(records)
    })
}

/* PUBLIC METHODS  */

let getSummary = function (activityId, callback) {
    getActivityXml(activityId, function (xml) {
        let activityStartTime = new Date(xml.gpx.metadata[0].time[0])
        callback({
            startTime: activityStartTime.toLocaleDateString()
        })
    })
}

let getDetail = function (activityId, callback) {
    getRecords(activityId, function (records) {
        let speedRecords = getSpeedRecords(records)
        let averageSpeed =  getAverageSpeed(speedRecords)
        let stopRecords = getStopRecords(records)

        callback({
            recordsCount: records.length,
            averageSpeed: averageSpeed,
            stopRecords: stopRecords,
            speedRecords: speedRecords
        })
    })
}

module.exports.getDetail = getDetail
module.exports.getSummary = getSummary
module.exports.getTimeDifferenceInMiliseconds = getTimeDifferenceInMiliseconds
module.exports.timeDifferenceIsBig = timeDifferenceIsBig